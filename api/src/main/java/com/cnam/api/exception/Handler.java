package com.cnam.api.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class Handler {

    @ExceptionHandler(Exception.class)
    protected ResponseEntity<Error> handleConflict(Exception ex) {

        if(ex instanceof NotFoundException){

            return new ResponseEntity<>(new Error(ex.getMessage(), HttpStatus.NOT_FOUND),HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(new Error(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR),HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
