package com.cnam.api.bdd.repository;

import com.cnam.api.bdd.model.Indice;
import com.cnam.api.bdd.model.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends CrudRepository<Role, Integer> {
    Role findByRole(String role);
}
