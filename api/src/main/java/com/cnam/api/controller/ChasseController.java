package com.cnam.api.controller;

import com.cnam.api.service.ChasseService;
import io.swagger.api.ChasseApi;
import io.swagger.model.Chasse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
public class ChasseController implements ChasseApi {

    @Autowired
    private ChasseService chasseService;

    @Override
    public ResponseEntity<Chasse> chasseCreate(@Valid Chasse chasse) {

        Chasse newChasse = chasseService.CreateChasse(chasse);

        return new ResponseEntity<>(newChasse, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<List<Chasse>> chasseFind() {

        List<Chasse> chasses = chasseService.getChasses();

        return new ResponseEntity<>(chasses, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Chasse> chasseGet(String chasseId) {

        Chasse chasse = chasseService.getChasse(chasseId);

        return new ResponseEntity<>(chasse, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> chasseDelete(String chasseId) {

        chasseService.deleteChasse(chasseId);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Override
    public ResponseEntity<Chasse> chasseUpdate(@Valid Chasse chasse, String chasseId) {

        Chasse newChasse = chasseService.updateChasse(chasse, chasseId);

        return new ResponseEntity<>(newChasse, HttpStatus.OK);
    }
}
