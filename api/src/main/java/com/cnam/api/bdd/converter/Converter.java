package com.cnam.api.bdd.converter;

public interface Converter< T, S > {

    S convertBddToModel(T dataBDD, S dataModel);
    T convertModelToBDD(S dataModel, T dataBDD);
}
