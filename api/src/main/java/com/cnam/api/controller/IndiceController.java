package com.cnam.api.controller;

import com.cnam.api.service.IndiceService;
import io.swagger.api.IndiceApi;
import io.swagger.model.Indice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
public class IndiceController implements IndiceApi {

    @Autowired
    IndiceService indiceService;

    @Override
    public ResponseEntity<Indice> indiceCreate(String chasseId, @Valid Indice indice) {

        Indice newIndice = indiceService.saveIndice(chasseId, indice);

        return new ResponseEntity<>(newIndice, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<List<Indice>> indiceFind(String chasseId) {

        List<Indice> indices = indiceService.getIndices(chasseId);

        return new ResponseEntity<>(indices, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Indice> indiceGet(String chasseId, String indiceId) {

        Indice indice = indiceService.getIndice(chasseId, indiceId);

        return new ResponseEntity<>(indice, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Indice> indiceUpdate(String chasseId, @Valid Indice indice, String indiceId) {

        Indice indiceUpdated = indiceService.updateIndice(chasseId, indice, indiceId);

        return new ResponseEntity<>(indiceUpdated, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Void> indiceDelete(String chasseId, String indiceId) {

        indiceService.deleteIndice(chasseId, indiceId);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
