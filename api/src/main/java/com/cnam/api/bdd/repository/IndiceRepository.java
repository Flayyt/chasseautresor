package com.cnam.api.bdd.repository;

import com.cnam.api.bdd.model.Chasse;
import com.cnam.api.bdd.model.Indice;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IndiceRepository extends CrudRepository<Indice, Integer> {

    List<Indice> findIndicesByChasse(Chasse chasse);
    Indice findIndiceByChasseAndIdHash(Chasse chasse, String idHash);
}
