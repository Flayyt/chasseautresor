package com.cnam.api.bdd.converter;

import com.cnam.api.bdd.model.Indice;
import io.swagger.model.Coordonnee;
import org.springframework.stereotype.Component;

@Component
public class IndiceConverter implements Converter<Indice, io.swagger.model.Indice>{
    @Override
    public io.swagger.model.Indice convertBddToModel(Indice dataBDD, io.swagger.model.Indice dataModel) {

        dataModel.setIdIndice(dataBDD.getIdHash());
        dataModel.setNom(dataBDD.getNom());
        dataModel.setTheme(dataBDD.getTheme());
        dataModel.setPiste(dataBDD.getTheme());
        dataModel.setMobile(dataBDD.getMobile());
        dataModel.setCode(dataBDD.getCode());
        dataModel.setDescription(dataBDD.getDescription());

        Coordonnee coordonnee = new Coordonnee();
        coordonnee.setLongitude(dataBDD.getLongitude());
        coordonnee.setLatitude(dataBDD.getLatitude());

        dataModel.setCoordonnee(coordonnee);

        return dataModel;
    }

    @Override
    public Indice convertModelToBDD(io.swagger.model.Indice dataModel, Indice dataBDD) {

        dataBDD.setIdHash(dataModel.getIdIndice());
        dataBDD.setNom(dataModel.getNom());
        dataBDD.setMobile(dataModel.isMobile());
        dataBDD.setPiste(dataModel.getPiste());
        dataBDD.setTheme(dataModel.getTheme());
        dataBDD.setDescription(dataModel.getDescription());
        dataBDD.setCode(dataModel.getCode());

        if(dataModel.getCoordonnee() != null){

            dataBDD.setLatitude(dataModel.getCoordonnee().getLatitude());
            dataBDD.setLongitude(dataModel.getCoordonnee().getLongitude());
        }

        return dataBDD;
    }
}
