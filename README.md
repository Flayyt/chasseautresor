Application "Chasse au trésor"

**Groupe** : Les auditeurs de l’impossible
**Membres** : Wheeler Mélissa, Seban Hugo, Péré Jean-Philippe, Beaumont Aurélien 

**Présentation** : Réaliser une application aidant à la logistique d'une équipe participant à une chasse au trésor. Cette application permettrait de créer plusieurs équipes privées qui pourraient signaler sur "leur" carte l'emplacement des indices trouvés, discuter ensemble et voir la position de leurs coéquipiers. L'organisateur de la chasse au trésor pourrait également envoyer des alertes à toutes les équipes.

Liste Uses case à faire :
UC10 : gestion des chasses aux trésors
UC11 : gestion des indices 
UC4 : Positionner un indice sur une carte
Authentification et habilitation 

Objectif de notre partie du projet :
	Proposer une api qui gère à la fois les chasses aux trésors ainsi que la création et le positionnement des indices.
Pour ce faire nous avons opté pour un SOA exposant une API REST sécurisée par un Microservice le tout sécurisé par un Gateway qui nous sert dans un premier temps à faire l’authentification  avant de nous réorienter vers le service et un registery service.
Notre travail est composé d’un docker pour la base de donné mysql sur le port 3636
un docker pour notre API sur le port 8080, le tout encapsulé dans un docker compose.

https://app.swaggerhub.com/apis/Flayyt/chasseAuTresor/0.0.1

etape a suivre :

lancer docker :
- sudo docker-compose up -d db pour la database mysql
- sudo docker-compose up -d --build



créer user :
	Clicker sur “Registration” : localhost:8080/registration
	Rentrer les données et valider
	Revenir sur la page d’accueil localhost:8080
	Renseigner les données de connection 
	Aller sur la page pour utilisateur connecté localhost:8080/admin/home
	
creer chasse


creer indice
