package com.cnam.api.bdd.converter;

import io.swagger.model.Coordonnee;
import io.swagger.model.Indice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ChasseConverter implements Converter<com.cnam.api.bdd.model.Chasse, io.swagger.model.Chasse>{

    @Autowired
    private IndiceConverter indiceConverter;

    @Override
    public io.swagger.model.Chasse convertBddToModel(com.cnam.api.bdd.model.Chasse dataBDD, io.swagger.model.Chasse dataModel){

        dataModel.setIdChasse(dataBDD.getIdHash());
        dataModel.setNom(dataBDD.getNom());
        dataModel.setDate(dataBDD.getDate());
        dataModel.setHeureDebut(dataBDD.getHeureDebut());
        dataModel.setHeureFin(dataBDD.getHeureFin());
        dataModel.setDescription(dataBDD.getDescription());

        Coordonnee coordonnee = new Coordonnee();
        coordonnee.setLatitude(dataBDD.getLatitude());
        coordonnee.setLongitude(dataBDD.getLongitude());
        dataModel.setLieu(coordonnee);

        List<Indice> indices = new ArrayList<>();
        for(com.cnam.api.bdd.model.Indice bddIndice : dataBDD.getIndices()){

            indices.add(indiceConverter.convertBddToModel(bddIndice, new Indice()));
        }
        dataModel.setIndices(indices);

        return dataModel;
    }

    @Override
    public com.cnam.api.bdd.model.Chasse convertModelToBDD(io.swagger.model.Chasse dataModel, com.cnam.api.bdd.model.Chasse dataBDD) {

        dataBDD.setIdHash(dataModel.getIdChasse());
        dataBDD.setLatitude(dataModel.getLieu().getLatitude());
        dataBDD.setLongitude(dataModel.getLieu().getLongitude());
        dataBDD.setNom(dataModel.getNom());
        dataBDD.setHeureDebut(dataModel.getHeureDebut());
        dataBDD.setHeureFin(dataModel.getHeureFin());
        dataBDD.setDate(dataModel.getDate());
        dataBDD.setDescription(dataModel.getDescription());

        List<com.cnam.api.bdd.model.Indice> bddIndices = new ArrayList<>();
        if(dataModel.getIndices() != null){

            for(Indice indice : dataModel.getIndices()){

                com.cnam.api.bdd.model.Indice bddIndice = indiceConverter.convertModelToBDD(indice, new com.cnam.api.bdd.model.Indice());
                bddIndice.setChasse(dataBDD);
                bddIndices.add(bddIndice);
            }
        }
        dataBDD.setIndices(bddIndices);

        return dataBDD;
    }
}
