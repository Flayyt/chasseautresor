package com.cnam.api.service;

import com.cnam.api.bdd.converter.ChasseConverter;
import com.cnam.api.dao.ChasseDAO;
import io.swagger.model.Chasse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Component
public class ChasseService {

    @Autowired
    protected ChasseDAO chasseDAO;

    @Autowired
    protected ChasseConverter chasseConverter;

    public Chasse CreateChasse(Chasse clientChasse){

        clientChasse.setIdChasse(UUID.randomUUID().toString());
        com.cnam.api.bdd.model.Chasse bddChasse = chasseConverter.convertModelToBDD(clientChasse, new com.cnam.api.bdd.model.Chasse());

        com.cnam.api.bdd.model.Chasse newBddChasse = chasseDAO.save(bddChasse);

        return chasseConverter.convertBddToModel(newBddChasse, new Chasse());
    }

    public List<Chasse> getChasses(){

        Iterable<com.cnam.api.bdd.model.Chasse> bddChasses = this.chasseDAO.find();

        List<Chasse> clientChasses = new ArrayList<>();
        for(com.cnam.api.bdd.model.Chasse bddChasse : bddChasses){

            clientChasses.add(chasseConverter.convertBddToModel(bddChasse, new Chasse()));
        }

        return clientChasses;
    }

    public Chasse getChasse(String chasseId) {

        com.cnam.api.bdd.model.Chasse chasseBdd = this.chasseDAO.get(chasseId);

        return chasseConverter.convertBddToModel(chasseBdd, new Chasse());
    }

    public Chasse updateChasse(Chasse clientNewChasse, String chasseId){

        com.cnam.api.bdd.model.Chasse bddChasseToChange = this.chasseDAO.get(chasseId);
        Chasse clientChasseToChange = chasseConverter.convertBddToModel(bddChasseToChange, new Chasse());

        if(clientNewChasse.getLieu() != null){

            clientChasseToChange.setLieu(clientNewChasse.getLieu());
        }
        if(clientNewChasse.getDate() != null){

            clientChasseToChange.setDate(clientNewChasse.getDate());
        }
        if(clientNewChasse.getNom() != null){

            clientChasseToChange.setNom(clientNewChasse.getNom());
        }
        if(clientNewChasse.getDescription() != null){

            clientChasseToChange.setDescription(clientNewChasse.getDescription());
        }
        if(clientNewChasse.getHeureDebut() != null){

            clientChasseToChange.setHeureDebut(clientNewChasse.getHeureDebut());
        }
        if(clientNewChasse.getHeureFin() != null){

            clientChasseToChange.setHeureFin(clientNewChasse.getHeureFin());
        }

        com.cnam.api.bdd.model.Chasse bddNewChasse = chasseConverter.convertModelToBDD(clientChasseToChange, new com.cnam.api.bdd.model.Chasse());
        bddNewChasse.setId(bddChasseToChange.getId());

        com.cnam.api.bdd.model.Chasse bddChasseUpdated = chasseDAO.update(bddNewChasse);

        return chasseConverter.convertBddToModel(bddChasseUpdated, new Chasse());
    }

    public void deleteChasse(String chasseId) {

        chasseDAO.delete(chasseId);
    }
}
