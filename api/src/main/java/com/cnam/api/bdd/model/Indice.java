package com.cnam.api.bdd.model;

import javax.persistence.*;

@Entity
public class Indice {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;

    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name = "id_hash", nullable = false)
    private String idHash;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "chasse_id")
    private Chasse chasse;

    private String nom;

    private String description;

    private String piste;

    private Boolean mobile;

    private String theme;

    private Float latitude;

    private Float longitude;

    private Integer code;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIdHash() {
        return idHash;
    }

    public void setIdHash(String idHash) {
        this.idHash = idHash;
    }

    public Chasse getChasse() {
        return chasse;
    }

    public void setChasse(Chasse chasse) {
        this.chasse = chasse;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPiste() {
        return piste;
    }

    public void setPiste(String piste) {
        this.piste = piste;
    }

    public Boolean getMobile() {
        return mobile;
    }

    public void setMobile(Boolean mobile) {
        this.mobile = mobile;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public Float getLatitude() {
        return latitude;
    }

    public void setLatitude(Float latitude) {
        this.latitude = latitude;
    }

    public Float getLongitude() {
        return longitude;
    }

    public void setLongitude(Float longitude) {
        this.longitude = longitude;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
}
