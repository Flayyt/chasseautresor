package com.cnam.api.service;

import com.cnam.api.bdd.converter.IndiceConverter;
import com.cnam.api.bdd.model.Chasse;
import com.cnam.api.dao.IndiceDAO;
import io.swagger.model.Indice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Component
public class IndiceService extends ChasseService {

    @Autowired
    private IndiceDAO indiceDAO;

    @Autowired
    private IndiceConverter indiceConverter;

    public Indice saveIndice(String chasseId, Indice clientIndice){

        Chasse bddChasse = this.chasseDAO.get(chasseId);

        clientIndice.setIdIndice(UUID.randomUUID().toString());
        com.cnam.api.bdd.model.Indice bddIndice = indiceConverter.convertModelToBDD(clientIndice, new com.cnam.api.bdd.model.Indice());
        bddIndice.setChasse(bddChasse);

        com.cnam.api.bdd.model.Indice bddNewIndice = indiceDAO.save(bddIndice);

        return indiceConverter.convertBddToModel(bddNewIndice, new Indice());
    }

    public List<Indice> getIndices(String chasseId){

        Chasse bddChasse = this.chasseDAO.get(chasseId);

        List<Indice> clientIndices = new ArrayList<>();
        for(com.cnam.api.bdd.model.Indice bddIndice : indiceDAO.find(bddChasse)){

            Indice clientIndice = indiceConverter.convertBddToModel(bddIndice, new Indice());
            clientIndices.add(clientIndice);
        }

        return clientIndices;
    }

    public Indice getIndice(String chasseId, String indiceId) {

        Chasse bddChasse = this.chasseDAO.get(chasseId);

        com.cnam.api.bdd.model.Indice bddIndice = indiceDAO.get(bddChasse, indiceId);

        return indiceConverter.convertBddToModel(bddIndice, new Indice());
    }

    public Indice updateIndice(String chasseId, Indice clientNewIndice, String indiceId){

        Chasse bddChasse = this.chasseDAO.get(chasseId);
        com.cnam.api.bdd.model.Indice bddIndiceToChange = this.indiceDAO.get(bddChasse, indiceId);

        Indice clientIndiceToChange = this.indiceConverter.convertBddToModel(bddIndiceToChange, new Indice());

        if(clientNewIndice.getTheme() != null ){

            clientIndiceToChange.setTheme(clientNewIndice.getTheme());
        }
        if(clientNewIndice.getPiste() != null){

            clientIndiceToChange.setPiste(clientNewIndice.getPiste());
        }
        if(clientNewIndice.getNom() != null){

            clientIndiceToChange.setNom(clientNewIndice.getNom());
        }
        if(clientNewIndice.getDescription() != null){

            clientIndiceToChange.setDescription(clientNewIndice.getDescription());
        }
        if(clientNewIndice.getCode() != null){

            clientIndiceToChange.setCode(clientNewIndice.getCode());
        }
        if(clientNewIndice.getCoordonnee() != null){

            clientIndiceToChange.setCoordonnee(clientNewIndice.getCoordonnee());
        }

        com.cnam.api.bdd.model.Indice bddIndiceUpdated = this.indiceConverter.convertModelToBDD(clientIndiceToChange, new com.cnam.api.bdd.model.Indice());
        bddIndiceUpdated.setId(bddIndiceToChange.getId());
        bddIndiceUpdated.setChasse(bddChasse);

        com.cnam.api.bdd.model.Indice bddNewIndice = this.indiceDAO.save(bddIndiceUpdated);

        return this.indiceConverter.convertBddToModel(bddNewIndice, new Indice());
    }

    public void deleteIndice(String chasseId, String indiceId) {

        Chasse bddChasse = this.chasseDAO.get(chasseId);

        indiceDAO.delete(bddChasse, indiceId);
    }
}
