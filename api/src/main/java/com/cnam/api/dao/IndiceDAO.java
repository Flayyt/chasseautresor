package com.cnam.api.dao;

import com.cnam.api.bdd.model.Chasse;
import com.cnam.api.bdd.model.Indice;
import com.cnam.api.bdd.repository.IndiceRepository;
import com.cnam.api.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class IndiceDAO {

    @Autowired
    private IndiceRepository indiceRepository;

    public Indice save(com.cnam.api.bdd.model.Indice bddIndice){

        return indiceRepository.save(bddIndice);
    }

    public Iterable<Indice> find(Chasse chasse){

        return indiceRepository.findIndicesByChasse(chasse);
    }

    public Indice get(Chasse chasse, String indiceId) {

        Indice indice = indiceRepository.findIndiceByChasseAndIdHash(chasse, indiceId);

        if(indice == null){

            throw new NotFoundException("l'identifiant "+ indiceId +" est inconnu");
        }

        return indice;
    }

    public void delete(Chasse chasse, String indiceId) {

        com.cnam.api.bdd.model.Indice bddIndice = this.indiceRepository.findIndiceByChasseAndIdHash(chasse, indiceId);

        indiceRepository.delete(bddIndice);
    }
}
