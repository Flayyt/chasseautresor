package com.cnam.api.dao;

import com.cnam.api.bdd.model.Chasse;
import com.cnam.api.bdd.repository.ChasseRepository;
import com.cnam.api.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ChasseDAO {

    @Autowired
    private ChasseRepository chasseRepository;

    public Chasse save(Chasse bddChasse){

        return chasseRepository.save(bddChasse);
    }

    public Iterable<Chasse> find(){

        return chasseRepository.findAll();
    }

    public Chasse get(String chasseId) {

        Chasse chasse = this.chasseRepository.findByIdHash(chasseId);

        if(chasse == null){

            throw new NotFoundException("l'identifiant "+ chasseId +" est inconnu");
        }

        return chasse;
    }

    public Chasse update(Chasse bddNewChasse){

        return this.chasseRepository.save(bddNewChasse);
    }

    public void delete(String chasseId) {

        com.cnam.api.bdd.model.Chasse bdChasse = this.chasseRepository.findByIdHash(chasseId);

        chasseRepository.delete(bdChasse);
    }
}
