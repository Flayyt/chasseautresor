package com.cnam.api.bdd.repository;

import com.cnam.api.bdd.model.Chasse;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ChasseRepository extends CrudRepository<Chasse, Integer> {

    Chasse findByIdHash(String chasseId);
}
